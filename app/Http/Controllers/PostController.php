<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use GuzzleHttp\Client;


class PostController extends Controller
{

    public function index()
    {

//        $posts = Post::all();

//        todo connect to api
        $client = new Client();
        $response = $client->get('http://127.0.0.1:8001');
        $posts = json_decode($response->getBody()->getContents(),true);
//        dd($posts);
        return view('posts.index', compact(
            'posts'
        ));
        //return json_encode($posts);
    }


    public function show($id)
    {
        $post = Post::find($id);
        return json_encode($post);
//        return view('posts.show', compact(
//            'post'
//        ));
    }
}
