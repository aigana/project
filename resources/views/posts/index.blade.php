<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chocolife</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cerulean/bootstrap.min.css" rel="stylesheet" integrity="sha384-zF4BRsG/fLiTGfR9QL82DrilZxrwgY/+du4p/c7J72zZj+FLYq4zY00RylP9ZjiT" crossorigin="anonymous">
    <style>
        .nav-def-cus{
            background-color: white;
            background-image: none;
            border-bottom: 1px solid #df2020;

        }

        .navbar-default .navbar-nav>li>a {
            color: black;
        }
        .navbar-header{
            padding-bottom: 5px;
        }

        .navbar-header img {
            width: 200px;
            height: 50px;
            margin-top: -15px;
        }

        .navbar-nav.menu {
            background-color: #df2020;
            color: black;
            margin-left: 50px;
            margin-top: 15px;
        }

        .navbar-inverse {
            background-color: #df2020;
            background-image: none;
            margin-top: 15px;
            border-bottom: none;
        }


        .caption {
            text-align: center;
        }

        .btn-primary {
            background-color: #df2020;
            background-image: none;
            border: none;
        }

        h3 {
            color: #df2020;
        }
    </style>
</head>
<body>
<div class="content">

    <nav class="navbar navbar-default navbar-fixed-top nav-def-cus">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>114
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('posts') }}"><img src ="../img/posts/chocologo.png"></a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Регистрация</a></li>
                    <li><a href="#">Вход</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <hr>

    <nav class="navbar navbar-inverse navbar-fixed">
        <div class="container-fluid">
            <ul class="nav navbar-nav menu ">
                <li><a href="#">Все</a></li>
                <li><a href="#">Новые</a></li>
                <li><a href="#">Хиты продаж</a></li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Развлечения и отдых <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"></a></li>
                        <li><a href="#">Активный отдых</a></li>
                        <li><a href="#">Бассейны</a></li>
                        <li><a href="#">Караоке</a></li>
                        <li><a href="#">Развлечения для детей</a></li>
                        <li><a href="#">Парки развлечений</a></li>
                        <li><a href="#">Кинотеатры</a></li>
                        <li><a href="#">Концерты</a></li>
                        <li><a href="#">Квесты</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Красота и здоровье<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">СПА услуги</a></li>
                        <li><a href="#">Массаж</a></li>
                        <li><a href="#">Уход за волосами</a></li>
                        <li><a href="#">Уход за ногтями</a></li>
                        <li><a href="#">Уход за лицом</a></li>
                        <li><a href="#">Солярий</a></li>
                        <li><a href="#">Коррекция фигуры</a></li>
                        <li><a href="#">Соляные пещеры</a></li>
                        <li><a href="#">Макияж и татуаж</a></li>
                        <li><a href="#">Прием к врачу</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Спорт<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Треажерный зал</a></li>
                            <li><a href="#">Кросс-фитнес</a></li>
                            <li><a href="#">Танцы</a></li>
                            <li><a href="#">Йога</a></li>
                            <li><a href="#">Единоборства</a></li>
                            <li><a href="#">Прокат и аренда</a></li>
                        </ul>
                        </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Товары<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Одежда и аксессуары</a></li>
                                <li><a href="#">Товары для детей</a></li>
                                <li><a href="#">Цветы и подарки</a></li>
                                <li><a href="#">Товары для здоровья</a></li>
                                <li><a href="#">Электроника</a></li>
                                <li><a href="#">Товары для дома</a></li>
                            </ul>
                            </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Услуги<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Химчистка</a></li>
                                    <li><a href="#">Обучающие курсы</a></li>
                                    <li><a href="#">Развивающие курся для детей</a></li>
                                    <li><a href="#">Языковые курсы</a></li>
                                    <li><a href="#">Фото, видео</a></li>
                                    <li><a href="#">Автоуслуги</a></li>
                                    <li><a href="#">Поможем вместе</a></li>
                                </ul>
                                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Еда<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Рестораны, кафе и пабы</a></li>
                                        <li><a href="#">Доставка</a></li>
                                        <li><a href="#">Продукты питания</a></li>
                                        <li><a href="#">Сеты</a></li>
                                    </ul>
                                    </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Туризм, отели<span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Санатории</a></li>
                                            <li><a href="#">Гостиницы и отели</a></li>
                                            <li><a href="#">Внутренний туризм</a></li>
                                            <li><a href="#">Отдых за границей</a></li>
                                            <li><a href="#">Боровое</a></li>
                                        </ul>
                                        </li>
                <li><a href="#">Бесплатные купоны</a></li>
            </ul>
        </div>
    </nav>




    <div class="container">
        <div class="row">
            @foreach($posts as $post)
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img height="200px" width="300px" src="{{ asset('img/posts/'.$post['img']) }}" alt="...">
                        <div class="caption">
                            <h3>{{ $post['title'] }}</h3>
                            <p>Цена от: {{ $post['price'] }} тг.</p>
                            <p>Цена со скидкой: {{ $post['discount'] }} тг.</p>

                            <p><a href="{{ route('post.show', $post['id']) }}" class="btn btn-primary" role="button">Подробнее</a></p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
</body>
</html>


