<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chocolife</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cerulean/bootstrap.min.css" rel="stylesheet" integrity="sha384-zF4BRsG/fLiTGfR9QL82DrilZxrwgY/+du4p/c7J72zZj+FLYq4zY00RylP9ZjiT" crossorigin="anonymous">
   <style>
       .nav-def-cus{
           background-color: white;
           background-image: none;
           border-bottom: 1px solid #df2020;

       }

       .navbar-default .navbar-nav>li>a {
           color: black;
       }
       .navbar-header{
           padding-bottom: 5px;
       }

       .navbar-header img {
           width: 200px;
           height: 50px;
           margin-top: -15px;
       }

       .navbar-nav.menu {
           background-color: #df2020;
           color: black;
           margin-left: 70px;
           margin-top: 15px;
       }

       .navbar-inverse {
           background-color: #df2020;
           background-image: none;
           margin-top: 15px;
           border-bottom: none;
       }


       .caption {
           text-align: center;
       }

       .btn-primary {
           background-color: #df2020;
           background-image: none;
           border: none;
       }

       h3 {
           color: #df2020;
       }
   </style>
</head>
<body>
<div class="content">

    <nav class="navbar navbar-default navbar-fixed-top nav-def-cus">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('posts') }}"><img src ="../img/posts/chocologo.png"></a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Регистрация</a></li>
                    <li><a href="#">Вход</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <hr>

    <nav class="navbar navbar-inverse navbar-fixed">
        <div class="container-fluid">
            <ul class="nav navbar-nav menu ">
                <li><a href="#">Все</a></li>
                <li><a href="#">Новые</a></li>
                <li><a href="#">Хиты продаж</a></li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Развлечения и отдых <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Page 1-1</a></li>
                        <li><a href="#">Page 1-2</a></li>
                        <li><a href="#">Page 1-3</a></li>
                    </ul>
                </li>
                <li><a href="#">Красота и здоровье</a></li>
                <li><a href="#">Спорт</a></li>
                <li><a href="#">Товары</a></li>
                <li><a href="#">Услуги</a></li>
                <li><a href="#">Еда</a></li>
                <li><a href="#">Туризм, отели</a></li>
                <li><a href="#">Бесплатные купоны</a></li>
            </ul>
        </div>
    </nav>




    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-4">
                        <img src="{{ asset("img/posts/".$post->img) }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <h3>{{ $post->title }}</h3>
                        <p>{{ $post->description}}</p>
                        <p>Цена от: {{ $post->price}}  тг.</p>
                        <p>Цена со скидкой: {{ $post->discount }} тг.</p>
                        <p>Купили: {{ $post->bought }} </p>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>



